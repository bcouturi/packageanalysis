from setuptools import setup

setup(
    name='PackageAnalysis',
    version='',
    packages=find_packages('python', exclude=['*.tests'] if version_info < (3, 0) else []),
    setup_requires=['networkx'],
    url='',
    license='',
    author='lben',
    author_email='',
    description=''
)
