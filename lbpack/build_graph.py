import logging
import networkx as nx
import os
import re
import sys

from collections import namedtuple

LbPackage = namedtuple('LbPackage', ['name', 'project', 'deps'], verbose=False)

paths = [("PHYS", "/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r3")]


def get_deps_from_cmakelists(filename):
    with open(filename) as f:
        data = f.read().replace("\n", "")
        logging.debug("Processing %s", filename)
        m = re.search("gaudi_depends_on_subdirs\((.*?)\)", data) # Non greedy match
        deps = []
        if m is not None:
            deps = m.group(1).split()
            #print deps
        return deps


def find_lbpackages(project, path):
    """ Recurse directory to find all CMakeLists.txt """
    lbpackages = []
    for root, subFolders, files in os.walk(path):
        if "CMakeLists.txt" in files:
            # We ignore the CMakeLists.txt in the top dir...
            if root != path:

                # Parsing the CMakeLists.txt to find deps
                deps = get_deps_from_cmakelists(os.path.join(root, "CMakeLists.txt"))
                name = root.replace(path, "")
                if name [0] == "/":
                    name = name[1:]
                lbpackages.append(LbPackage(name, project, deps=deps))
    return lbpackages


def main():

    g = nx.DiGraph()
    for project, path in paths:
        packs = find_lbpackages(project, path)
        for p in packs:
            g.add_node(p.name, project=p.project)
            for d in p.deps:
                print d
                g.add_edge(p.name, d)

    print(g)

if __name__ == "__main__":
    main()
